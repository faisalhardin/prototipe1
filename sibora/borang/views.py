from borang.models import Borang
from borang.serializers import BorangSerializer
from rest_framework import generics
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404


# from rest_framework import status
# from rest_framework.decorators import api_view
from rest_framework.response import Response
# from borang.models import Borang
# from borang.serializers import BorangSerializer
# from rest_framework.renderers import TemplateHTMLRenderer
# from rest_framework import generics
# @api_view(['GET', 'POST'])
# def borang_list(request, format=None):
#     if request.method == 'GET':
#         borangs = Borang.objects.all()
#         serializer = BorangSerializer(borangs, many=True)
#         return Response(serializer.data)

#     elif request.method == 'POST':
#         serializer = BorangSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# @api_view(['GET', 'PUT', 'DELETE'])
# def borang_detail(request, pk, format=None):
#     try:
#         borang = Borang.objects.get(pk=pk)
#     except Borang.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
    
#     if request.method == 'GET':
#         serializer = BorangSerializer(borang)
#         return Response(serializer.data)

#     elif request.method == 'PUT':
#         serializer = BorangSerializer(borang, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     elif request.method == 'DELETE':
#         borang.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# Create your views here.

class BorangList(generics.ListCreateAPIView):
    queryset = Borang.objects.all()
    serializer_class = BorangSerializer

class BorangDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Borang.objects.all()
    serializer_class = BorangSerializer
    # renderer_classes = [TemplateHTMLRenderer]
    # template_name = 'borang/borang.html'

    # def retrieve(self, request, pk, *args, **kwargs):
    #     borang = get_object_or_404(Borang, pk=pk)
    #     serializer = BorangSerializer(borang)
    #     return Response({'serializer':serializer, 'borang':borang })


class TemplateBorangDetail(BorangDetail):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'borang/borang.html'

    def retrieve(self, request, pk, *args, **kwargs):
        borang = get_object_or_404(Borang, pk=pk)
        serializer = BorangSerializer(borang)
        return Response({'borang':borang })
# class ProfileList(APIView, BorangDetail):
#     renderer_classes = [TemplateHTMLRenderer]
#     template_name = 'borang/borang.html'

#     def get(self, request):
#         queryset = Borang.objects.all()
#         return Response({'borang': queryset})