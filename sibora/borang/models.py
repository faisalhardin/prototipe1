from django.db import models

# Create your models here.
class  Borang(models.Model):
    nama = models.CharField(max_length=20, blank=False)
    npm = models.CharField(max_length=10, blank=False)
    penilaian = models.TextField()
    