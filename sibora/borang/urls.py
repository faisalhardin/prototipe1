from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from borang import views

urlpatterns = [
    url(r'^borangs/$', views.BorangList.as_view()),
    url(r'^borangs/(?P<pk>[0-9]+)/$', views.BorangDetail.as_view()),
    url(r'^borangs-template/(?P<pk>[0-9]+)/$', views.TemplateBorangDetail.as_view()),
    # url(r'^borangs-view/(?P<pk>[0-9]+)/$', views.NamaDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)