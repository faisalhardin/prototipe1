from rest_framework import serializers
from borang.models import Borang

class BorangSerializer(serializers.ModelSerializer):
    class Meta:
        model = Borang
        fields = ('id','nama', 'npm','penilaian')